import api from "@/service/api.js";
import toast from "@/lib/toast.js";
const state = {
    entries: [],
    r: 1
};
const getters = {
    ENTRIES: state => {
        return state.entries;
    }
};
const mutations = {
    ADD_ENTRY: (state, entry) => {
        state.entries.push(entry);
    },
    SET_ENTRIES: (state, payload) => {
        state.entries = payload;
    }
};
const actions = {
    GET_ENTRIES: async context => {
        api()
            .get("/api/dots/getAll")
            .then(res => {
                context.commit("SET_ENTRIES", res.data);
            })
            .catch(err => {
                toast.error("Could not load history: " + err.message);
            });
    },
    POST_ENTRY: async (context, payload) => {
        api()
            .post("/api/dots/add", payload)
            .then((res) => {
                let flag;
                JSON.parse(JSON.stringify(res.data), function (key, value) {
                    if (key === 'inarea') {
                        flag = new Boolean(value);
                    }
                })
                let entry = {
                    x: res.data.x,
                    y: res.data.y,
                    r: res.data.r,
                    inarea: flag
                };

                if (entry.inarea == true) {
                    toast.success("Correct!");
                } else {
                    toast.error("Wrong!");
                }

                context.commit("ADD_ENTRY", entry);
            })
            .catch(err => {
                toast.error(err.message);
            });
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
